from django.http import HttpResponse
from django.shortcuts import render
from django.utils import timezone
from . import models
from .serializers import ClientSerializer, MailingSerializer, MessageSerializer
from . models import Client, Mailing, Message
from .tasks import send_message
from rest_framework import generics


class ClientListCreateView(generics.ListCreateAPIView):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer


class ClientRetrieveUpdateDestroyView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer


class MailingListCreateView(generics.ListCreateAPIView):
    queryset = Mailing.objects.all()
    serializer_class = MailingSerializer


class MailingRetrieveUpdateDestroyView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Mailing.objects.all()
    serializer_class = MailingSerializer


class MessageListCreateView(generics.ListCreateAPIView):
    queryset = Message.objects.all()
    serializer_class = MessageSerializer


class MessageRetrieveUpdateDestroyView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Message.objects.all()
    serializer_class = MessageSerializer


class MailingStatisticsView(generics.ListAPIView):
    serializer_class = MailingSerializer

    def get_queryset(self):
        return Mailing.objects.annotate(message_count=models.Count('message')).values('id', 'status', 'message_count')


class MailingMessageStatisticsView(generics.ListAPIView):
    serializer_class = MessageSerializer

    def get_queryset(self):
        mailing_id = self.kwargs['mailing_id']
        return Message.objects.filter(mailing_id=mailing_id)


def process_mailings(request):
    current_time = timezone.now()
    active_mailings = Mailing.objects.filter(start_time__lte=current_time, end_time__gt=current_time)

    for mailing in active_mailings:
        clients = Client.objects.filter(
            operator_code=mailing.client_filter_operator_code,
            tag=mailing.client_filter_tag
        )

        for client in clients:
            message = Message.objects.create(
                mailing=mailing,
                client=client
            )

            send_message.delay(message.id)

    return HttpResponse("Mailings processing started.")
