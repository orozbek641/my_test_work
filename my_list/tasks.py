from celery import shared_task
from .models import Message


@shared_task
def send_message(message_id):
    try:
        message = Message.objects.get(id=message_id)
        message.status = 'Отправлено'
        message.save()
    except Message.DoesNotExist:
        pass
