from django.urls import path

from . import views

urlpatterns = [
    path('clients/', views.ClientListCreateView.as_view(), name='client-list-create'),
    path('clients/<int:pk>/', views.ClientRetrieveUpdateDestroyView.as_view(), name='client-retrieve-update-destroy'),
    path('mailings/', views.MailingListCreateView.as_view(), name='mailing-list-create'),
    path('mailings/<int:pk>/', views.MailingRetrieveUpdateDestroyView.as_view(), name='mailing_re'),
    path('process-mailings/', views.process_mailings, name='process-mailings'),
    ]